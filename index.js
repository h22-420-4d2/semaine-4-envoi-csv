const express = require('express');
const db = require('./modules/db');

const app = express();
const port = 3000;

app.use(express.static('public'));

app.get('/notes-csv', async (req, res) => {
  // 1ère partie: faites votre requête SQL pour aller chercher la liste des données
  // La requête suivante est incomplète car elle ne récupère pas toutes les colonnes voulues!
  const results = await db('results');

  // 2e partie: construction du cvs
  // Attention! Vous devez adapter le script aux données voulues!
  const cvsHeader = ['id étudiant', 'id évaluation', 'note'];

  const csvRows = [cvsHeader, ...results.map((row) => {
    const array = [row.student_id, row.eval_id, row.note];
    return array.join(';');
  })];

  const csvData = csvRows.join('\n');

  // 3e partie: envoi en tant que fichier. Répondez à la question de la ligne 27
  res
    .attachment('resultat.csv') // Que fait cette méthode?? répondez ici!
    .send(csvData);
});

app.listen(port, () => {
  console.log(`Cette application web peut maintenant recevoir des requêtes: http://localhost:${port}`);
});
